const compareWithModel = (model, body) => {
    let data = {};
    for (const key in body) {
        if (model.hasOwnProperty(key)) {
            data[key] = body[key];
        }
    }
    return data;
}

exports.compareWithModel = compareWithModel;