const alphanumeric = (data) => {
  let letters = /^[A-Za-z\'\-]*$/g;
  if (data === undefined) {
    return false;
  }
  return !letters.test(data);
}

const validateGmail = (data) => {
  let testData = /^[\w.+\-]+@gmail\.com$/g;
  if (data === undefined) {
    return false;
  }
  return !testData.test(data);
}

const validatePhone = (data) => {
  let testData = /^\+380?([5-9][0-9]\d{7})$/;
  if (data === undefined) {
    return false;
  }
  return !testData.test(data);
}

const validateLenght = (data, length = 3) => {
  if (data === undefined) {
    return false;
  }
  return !(data.length >= length);
}

exports.alphanumeric = alphanumeric;
exports.validateGmail = validateGmail;
exports.validatePhone = validatePhone;
exports.validateLenght = validateLenght;