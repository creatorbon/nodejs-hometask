const responseMiddleware = (req, res, next) => {
    // TODO: Implement middleware that returns result of the query
    if (res.err) res.send(res.err);
    else res.send(res.data);
    next();
}

exports.responseMiddleware = responseMiddleware;