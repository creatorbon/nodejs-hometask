const { user } = require('../models/user');
const { compareWithModel } = require('../helpers/compareWithModel')
const { alphanumeric, validateGmail, validatePhone, validateLenght } = require('../helpers/validate')

const createUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during creation
    const data = req.body;

    if (alphanumeric(data.firstName)) {
        res.status("400").send({ error: true, message: "First Name field have invalid characters" });
    } else if (data.firstName === undefined || validateLenght(data.firstName, 2)) {
        res.status("400").send({ error: true, message: "First Name field less than 2 characters" });
    } else if (alphanumeric(data.lastName)) {
        res.status("400").send({ error: true, message: "Last Name field have invalid characters" });
    } else if (data.lastName === undefined || validateLenght(data.lastName, 2)) {
        res.status("400").send({ error: true, message: "Last Name field less than 2 characters" });
    } else if (data.password === undefined || validateLenght(data.password)) {
        res.status("400").send({ error: true, message: "Password must be at least 3 characters" });
    } else if (validatePhone(data.phoneNumber)) {
        res.status("400").send({ error: true, message: "Invalid Ukrainian phone number" });
    } else if (data.email === undefined || validateLenght(data.email)) {
        res.status("400").send({ error: true, message: "Email field is required" });
    } else if (validateGmail(data.email)) {
        res.status("400").send({ error: true, message: "Invalid Gmail address" });
    } else {
        req.body = compareWithModel(user, {...data, email: data.email.toLowerCase()});
        next();
    }
}

const updateUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during update
    const data = req.body;

    if (alphanumeric(data.firstName)) {
        res.status("400").send({ error: true, message: "First Name field have invalid characters" });
    } else if (validateLenght(data.firstName, 2)) {
        res.status("400").send({ error: true, message: "First Name field less than 2 characters" });
    } else if (alphanumeric(data.lastName)) {
        res.status("400").send({ error: true, message: "Last Name field have invalid characters" });
    } else if (validateLenght(data.lastName, 2)) {
        res.status("400").send({ error: true, message: "Last Name field less than 2 characters" });
    } else if (validateLenght(data.password)) {
        res.status("400").send({ error: true, message: "Password must be at least 3 characters" });
    } else if (validatePhone(data.phoneNumber)) {
        res.status("400").send({ error: true, message: "Invalid Ukrainian phone number" });
    } else if (validateGmail(data.email)) {
        res.status("400").send({ error: true, message: "Invalid Gmail address" });
    } else {
        req.body = !!data.email ? compareWithModel(user, {...data, email: data.email.toLowerCase()}) :  compareWithModel(user, data)
        next();
    }

}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;