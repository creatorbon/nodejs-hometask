const { fighter } = require('../models/fighter');
const { compareWithModel } = require('../helpers/compareWithModel');
const { alphanumeric, validateLenght } = require('../helpers/validate');

const createFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during creation
    let data = req.body;

    if (alphanumeric(data.name)) {
        res.status("400").send({ error: true, message: "Fighter Name have invalid characters" });
    } else if (data.name === undefined || validateLenght(data.name, 2)) {
        res.status("400").send({ error: true, message: "Fighter Name less than 2 characters" });
    } else if (data.defense === undefined || 0 === +data.defense || +data.defense > 10) {
        res.status("400").send({ error: true, message: "The defense must be between 1 and 10" });
    } else if (data.power === undefined || 0 === +data.power || +data.power > 100) {
        res.status("400").send({ error: true, message: "The power must be between 1 and 100" });
    } else {
        req.body = compareWithModel(fighter, {...data, health: 100 });
        next();
    }
}

const updateFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during update
    let data = req.body;
    
    if (alphanumeric(data.name)) {
        res.status("400").send({ error: true, message: "Fighter Name have invalid characters" });
    } else if (validateLenght(data.name, 2)) {
        res.status("400").send({ error: true, message: "Fighter Name less than 2 characters" });
    } else if (0 === +data.defense || +data.defense > 10) {
        res.status("400").send({ error: true, message: "The defense must be between 1 and 10" });
    } else if (0 === +data.power || +data.power > 100) {
        res.status("400").send({ error: true, message: "The power must be between 1 and 100" });
    } else {
        req.body = compareWithModel(fighter, data);
        next();
    }
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;