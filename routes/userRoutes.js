const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

// TODO: Implement route controllers for user
router.get('/', (req, res, next) => {
    try {
        let data = UserService.getAll();
        res.data = data;
    } catch (err) {
        res.err = { error: true, message: err.message };
        res.status(404);
    } finally {
        next();
    }
}, responseMiddleware);

router.get('/:id', (req, res, next) => {
    try {
        let id = req.params.id;
        let data = UserService.search({ "id": id });
        
        if (!data) {
            res.status(404);
            res.err = { error: true, message: 'User not found' };
        } else {
            res.data = data;
        }
    } catch (err) {
        res.err = { error: true, message: err.message };
        res.status(404);
    } finally {
        next();
    }
}, responseMiddleware);

router.post('/', createUserValid, (req, res, next) => {
    try {
        let candidateEmail = UserService.search({ email: req.body.email });
        let candidatePhone = UserService.search({ phoneNumber: req.body.phoneNumber });

        if (candidateEmail) {
            res.err = { error: true, message: "User with this email already registered." };
            res.status(400);
        } else if (candidatePhone) {
            res.err = { error: true, message: "User with this phone number already registered." };
            res.status(400);
        } else {
            let data = UserService.create(req.body);
            res.data = data;
        }
    } catch (err) {
        res.err = { error: true, message: err.message };
        res.status(404);
    } finally {
        next();
    }
}, responseMiddleware);

router.put('/:id', updateUserValid, (req, res, next) => {
    try {
        let candidateEmail = UserService.search({ email: req.body.email });
        let candidatePhone = UserService.search({ phoneNumber: req.body.phoneNumber });

        if (candidateEmail) {
            res.err = { error: true, message: "User with this email already registered." };
            res.status(400);
        } else if (candidatePhone) {
            res.err = { error: true, message: "User with this phone number already registered." };
            res.status(400);
        } else {
            let id = req.params.id;
            let data = UserService.update(id, req.body);
            res.data = data
        }
    } catch (err) {
        res.err = { error: true, message: err.message };
        res.status(404);
    } finally {
        next();
    }
}, responseMiddleware);

router.delete('/:id', (req, res, next) => {
    try {
        let id = req.params.id;
        let data = UserService.delete(id);
        if (!data) {
            res.status(404);
            res.err = { error: true, message: 'User not found' };
        } else {
            res.data = {};
        }
    } catch (err) {
        res.err = { error: true, message: err.message };
        res.status(404);
    } finally {
        next();
    }
}, responseMiddleware);

module.exports = router;