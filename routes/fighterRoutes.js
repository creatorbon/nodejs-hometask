const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

// TODO: Implement route controllers for fighter

router.get('/', (req, res, next) => {
    try {
        let data = FighterService.getAll();
        res.data = data;
    } catch (err) {
        res.err = { error: true, message: err.message };
        res.status(404)
    } finally {
        next();
    }
}, responseMiddleware);

router.get('/:id', (req, res, next) => {
    try {
        let id = req.params.id;
        let data = FighterService.search({ "id": id });

        if (!data) {
            res.status(404)
            res.err = { error: true, message: 'User not found' };
        } else {
            res.data = data
        }
    } catch (err) {
        res.err = { error: true, message: err.message };
        res.status(404);
    } finally {
        next();
    }
}, responseMiddleware);

router.post('/', createFighterValid, (req, res, next) => {
    try {
        let candidateName = FighterService.search({ name: req.body.name });

        if (candidateName) {
            res.err = { error: true, message: "Fighter with this name already have" };
            res.status(400);
        } else {
            let data = FighterService.create(req.body);
            res.data = data;
        }
    } catch (err) {
        res.err = { error: true, message: err.message };
        res.status(404);
    } finally {
        next();
    }
}, responseMiddleware);

router.put('/:id', updateFighterValid, (req, res, next) => {
    try {
        let candidateName = FighterService.search({ name: req.body.name })

        if (candidateName) {
            res.err = { error: true, message: "Fighter with this name already have" };
            res.status(400);
        } else {
            let id = req.params.id;
            let data = FighterService.update(id, req.body);
            res.data = data;
        }
    } catch (err) {
        res.err = { error: true, message: err.message };
        res.status(404);
    } finally {
        next();
    }
}, responseMiddleware);

router.delete('/:id', (req, res, next) => {
    try {
        let id = req.params.id;
        let data = FighterService.delete(id);
        if (!data) {
            res.status(404);
            res.err = { error: true, message: 'User not found' };
        } else {
            res.data = {};
        }
    } catch (err) {
        res.err = { error: true, message: err.message };
        res.status(404);
    } finally {
        next();
    }
}, responseMiddleware);

module.exports = router;