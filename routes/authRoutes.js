const { Router } = require('express');
const AuthService = require('../services/authService');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.post('/login', (req, res, next) => {
    try {
        // TODO: Implement login action (get the user if it exist with entered credentials)
        const { email, password } = req.body;
        
        if (!password) {
            res.err = { error: true, message: "Password field is required" };
            res.status(400);
        } else if (!email) {
            res.err = { error: true, message: "Email field is required" };
            res.status(400);
        } else {
            let data = AuthService.login(req.body);
            res.data = data;
        }
    } catch (err) {
        res.err = { error: true, message: err.message };
        res.status(404);
    } finally {
        next();
    }
}, responseMiddleware);

module.exports = router;